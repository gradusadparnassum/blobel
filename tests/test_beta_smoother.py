import pandas as pd
import numpy as np
from kvit.feature_transformer import beta_smoother
import pytest


@pytest.mark.skip
def test_beta_smoother():
    df = pd.read_csv(
        'https://statweb.stanford.edu/~tibs/ElemStatLearn/datasets/SAheart.data', index_col=0)

    beta_smoother(frame=df, feature='tobacco', target='chd', n_bins=10)
