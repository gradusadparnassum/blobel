import numpy as np

from kvit.linear import LinearRegressionModel
from kvit.linear import linear_regression_model
from kvit.linear import linear_regression_model_with_uncertainties

from hypothesis import strategies as st
from hypothesis import given

from numpy.testing import assert_allclose
from kvit.linear import bayesian_linear_regression_model


def test_univariate_weighted_linear_regression_fits_although_outlier_present():
    np.random.seed(0)
    N = 100
    x = np.linspace(1, 10., N)
    s = np.ones_like(x)
    s[2] = 9
    y = 3 * x + 4
    y[2] = 10

    features = np.c_[x]

    model = linear_regression_model_with_uncertainties(features, y, s)
    assert_allclose(model.coefficients, np.c_[[4, 3]], atol=0.1)

    y_estimated = model.estimate(features)
    assert y_estimated.ndim == 1
    assert y_estimated.shape == (N, )

    variance = model.covariance_estimate(x)
    assert variance.shape == (N, N)


@given(a=st.floats(min_value=2.5, max_value=3.5), b=st.floats(min_value=3.5, max_value=4.5))
def test_univariate_linear_regression(a, b):
    np.random.seed(0)
    x = np.random.uniform(0, 10, size=10)
    y = 3 * x + 4.0

    features = np.c_[x]

    model = linear_regression_model(features, y)

    assert_allclose(model.coefficients, np.c_[[4, 3]], atol=0.25)

    estimate = model.estimate(features)

    residual_fit = np.abs(estimate - y)

    absolute_deviation = np.mean(np.abs(residual_fit))
    assert absolute_deviation < 10

    alt_estimate = a * x + b

    residual_alternative = np.abs(alt_estimate - y)

    deviation_fit = np.inner(residual_fit, y)
    deviation_alt = np.inner(residual_alternative, y)
    assert deviation_fit < deviation_alt


def test_bayesian_linear_regression():
    N = 10
    beta = 0.1
    coefficients = [1.4, 2]
    x = np.linspace(0, 1, N)
    y = np.poly1d(coefficients)(x) + np.random.normal(scale=beta, size=N)
    model = bayesian_linear_regression_model(x, y, alpha=1e-15, beta=beta)
    np.testing.assert_allclose(model.coefficients[::-1], coefficients, atol=0.1)
