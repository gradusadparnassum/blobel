from kvit.ortho import orthogonal_polynomials
from kvit.ortho import orthogonal_fit

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import pytest


def assert_polygons_are_orthogonal(x, w, polygons, atol):
    __tracebackhide__ = True
    kronecker = np.zeros((len(polygons), len(polygons)), dtype=float)

    for i, p1 in enumerate(polygons):
        for j, p2 in enumerate(polygons):
            kronecker[i, j] = np.inner(w, (p1 * p2)(x))

    due = np.eye(len(polygons))
    print(np.round(kronecker, 2))

    np.testing.assert_allclose(due, kronecker, atol=atol)


def test_orthogonal_polygons_basis():
    x = np.arange(-9, 10.)
    w = np.ones_like(x)
    ps = orthogonal_polynomials(x, w)

    assert_polygons_are_orthogonal(x, w, ps[:5], atol=1e-4)


@pytest.mark.skip(reason="not orthonormal yet, TODO")
def test_orthogonal_polygons():
    x = np.arange(7.)
    w = np.ones_like(x)
    ps = orthogonal_polynomials(x, w)

    assert_polygons_are_orthogonal(x, w, ps, atol=1e-4)

    y = 0

    coefficients = [15., 10., 100., 30., 1., 3., 1.]

    for a, p in zip(coefficients, ps):
        y += a * p

    y = y(x)

    coefficients_estimated = orthogonal_fit(x, y, w)
    np.testing.assert_allclose(coefficients_estimated, coefficients)


def test_ortho_predict():
    x = 15 * np.random.randn(10000)
    y = 1.0 / (1 + np.exp(-x)) + 0.2 * np.random.randn(len(x))
    plt.figure()
    plt.plot(x, y, ",")
    plt.savefig("raw.png")
    plt.close("all")

    df = pd.DataFrame({"x": x, "y": y})
    df["n"] = pd.qcut(x, q=20).codes

    mv = df.groupby(["n"])["y"].agg(["mean", "var"])
    mv["n"] = mv.index

    x = mv["n"].values
    y = mv["mean"].values
    variance = mv["var"].values

    w = 1. / variance

    x_test = np.linspace(x.min(), x.max())

    plt.errorbar(x, y, yerr=variance, fmt="-o", label="data")

    result = []
    for order in range(2, 8):
        model = orthogonal_fit(x, y, w, order=order)
        y_predicted = model.estimate(x_test)
        # plt.plot(x_test, y_predicted, "-", label="fit $O(x^{})$".format(order))
        y_p = model.estimate(x)
        mad = np.mean(np.abs(y_p - y))
        print("MAD", mad)
        result.append(mad)
