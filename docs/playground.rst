==========
Playground
==========



Profile Plots
=============



.. plot::
   :include-source:
   :context:

   import pandas as pd
   import numpy as np
   df = pd.read_csv('https://statweb.stanford.edu/~tibs/ElemStatLearn/datasets/SAheart.data', index_col=0)

   from kvit.feature_transformer import beta_smoother


.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='tobacco', target='chd', n_bins=10)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='age', target='chd', n_bins=20)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='alcohol', target='chd', n_bins=10)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='sbp', target='chd', n_bins=20)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='adiposity', target='chd', n_bins=20)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='famhist', target='chd', n_bins=20)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='typea', target='chd', n_bins=20)

.. plot::
   :include-source:
   :context: close-figs

   beta_smoother(frame=df, feature='obesity', target='chd', n_bins=20)
