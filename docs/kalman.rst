===========================
Linear Quadratic Estimation
===========================

Linear Quadratic Estimation, best known under the name *Kalman* filter, or
*Kalman* smoother, is an time-series algorithm for observing.

Background
==========

The general problem of the Kalman filter is, that we would like to estimate an
unknown quantity :math:`\vec{z}_k` (the latent variable), when we only have
noisy observations :math:`\vec{x}_k` and a relationship between noisy
observations and the quantity of interest :cite:`2006:bishop`:


.. math::

   \wp\left(\vec{x}_n\mid\vec{z}_n\right) = \mathcal{N}\left(\vec{x}_n\mid C\vec{z}_n,\Sigma\right)


For the latent variable, the Kalman filter assumes, that we can develop it in
time

.. math::

   \wp\left(\vec{z}_n\mid\vec{z}_{n-1}\right) = \mathcal{N}\left(\vec{z}_n\mid A\vec{z}_{n-1},\Gamma\right)


For an initial estimate of the latent variable :math:`\vec{z}_k`, we can assume

.. math::

  \wp(\vec{z}_1) = \mathcal{N}\left(\vec{z}_1\mid \vec{\mu}_0, V_0\right)


Inference
---------

.. math::

   P_{n-1} &= AV_{n-1}A^T + \Gamma

   \mu_n &= A\mu_{n_1} + K_n(\vec{x}_n - CA\vec{\mu}_{n-1})

   V_n &= (I - K_n C)P_{n-1}

   K_n &= P_{n-1}C^T \left( CP_{n-1} C^T + \Sigma\right)^{-1}


Example
=======

.. plot::
   :include-source:
   :context:

   >>> import numpy as np
   >>> from matplotlib import pyplot as plt
   >>> t = np.linspace(0, 5, 40)
   >>> h0 = 120
   >>> g = 9.81
   >>> v = 1
   >>> x1_signal = v * t
   >>> x2_signal = h0 - 0.5 * g * t**2
   >>> x1_noise = 0.2 * np.random.randn(len(t))
   >>> x2_noise = 0.2 * np.random.randn(len(t))

   >>> x1 = x1_signal + x1_noise
   >>> x2 = x2_signal + x2_noise

   >>> _ = plt.plot(x1, x2, "o")
   >>> _ = plt.title("Falling Ball")

.. plot::
   :include-source:
   :context: close-figs

   >>> _ = plt.plot(t, x1, "o")
   >>> _ = plt.plot(t, x2, "o")

Kalman
------

.. plot::
   :include-source:
   :context: close-figs

   >>> dt = t[1] - t[0]
   >>> F = np.c_[[1, 0], [dt, 1]]
   >>> G = np.c_[[0.5 * dt**2, dt]]
   >>> H = np.c_[[1, 0]]

   >>> from kvit.smoothing import kalman
   >>> x = np.c_[x1, x2]
   >>> z = kalman(x, F, G, H)
   >>> _ = plt.plot(x1, x2, "o")
   >>> _ = plt.plot(z[:, 0], z[:, 1])
