import numpy as np

from functools import reduce
import operator


def weighted_mean(x, w):
    return np.inner(w, x) / np.sum(w)


x_ = np.poly1d([1, 0])


def orthogonal_polynomials(x, w):
    if (w <= 0).any():
        raise ValueError("weights must be positive and not-equal 0, got{}".format(w))
    wm = weighted_mean(x, w)

    b11 = (np.inner(w, (x - wm)**2))**-0.5
    b00 = np.sum(w)**-0.5
    b10 = -b11 * wm

    polys = []
    polys.append(np.poly1d([b00]))
    polys.append(np.poly1d([b11, b10]))

    order = len(x)

    for i in range(order - 2):
        poly_for_alpha = x_ * polys[-1]**2
        alpha = np.inner(w, poly_for_alpha(x))
        poly_for_beta = x_ * polys[-1] * polys[-2]
        beta = np.inner(w, poly_for_beta(x))
        poly_for_gamma = (((x_ - alpha) * polys[-1]) - beta * polys[-2])(x)
        gamma = np.sqrt(np.inner(w, poly_for_gamma**2))
        polys.append(((x_ - alpha) * polys[-1] - beta * polys[-2]) / gamma)

    return polys


def orthogonal_fit(x, y, w, *, order=None):
    if not order:
        order = np.clip(len(y) // 5, 2, 21)
    polynomials = orthogonal_polynomials(x, w)
    weighted_y = weighted_mean(y, w)

    sum_squared = np.inner(w, (y - weighted_y)**2)
    deltas = []
    for poly in polynomials:
        delta = np.inner(w, poly(x) * y)
        sum_squared = max(0, sum_squared - delta * delta)
        deltas.append(delta)

    assert len(polynomials) == len(deltas)
    acc = np.poly1d([0.0])
    for d, p in zip(deltas[:order], polynomials[:order]):
        acc += p * d
    assert type(acc) == np.poly1d
    return OrthogonalModel(fit=acc)


class OrthogonalModel:
    """A model representing a fitted orthogonal regression"""

    def __init__(self, *, fit):
        self._fit = fit

    def estimate(self, x):
        """Estimate a target, given predictors :var:`x`"""
        return self._fit(x)
