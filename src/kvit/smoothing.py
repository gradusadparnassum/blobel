import numpy as np


def kalman(x, F, G, H):
    if x.ndim == 1:
        x = x.reshape((-1, 1))

    n_events, rows = x.shape

    z = np.ones((n_events, rows))

    for i in range(n_events):
        z[i, :] = x[i, :]


    return z


__all__ = ['kalman']
