;; Blobel: cooperative, event-driven user-space threads.

;;;; Copyright (C) 2020 Holger Peters <email@holger-peters.de>
;;;;
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(use-modules (ice-9 popen)
             (ice-9 match)
             (ice-9 rdelim)
             (srfi srfi-1)
             (srfi srfi-26)
             (guix build-system python)
             (guix gexp)
             (guix git-download)
             (guix licenses)
             (guix packages)
             (gnu packages)
             (gnu packages check)
             (gnu packages python-check)
             (gnu packages python-xyz))

(define %source-dir (dirname (current-filename)))

(define python-blobel
  (package
    (name "python-blobel")
    (version "git")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system python-build-system)
    (native-inputs
     `(("python-pytest-flake8" ,python-pytest-flake8)
       ("python-pytest-cov" ,python-pytest-cov)
       ("python-pytest-runner" ,python-pytest-runner)))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)))
    (synopsis "obscure but useful statistic routines")
    (description "Blobel is a library")
    (home-page "https://codeberg.org/gradusadparnassum/blobel")
    (license lgpl3+)))

python-blobel
